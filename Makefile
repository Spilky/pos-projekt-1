CC=gcc
CFLAGS=-std=c99 -Wall -pedantic

proj01: proj01.c
	$(CC) $(CFLAGS) $^ -o $@

clean:
	rm -f *.o proj01
