//
// Created by David Spilka (xspilk00) on 15.4.16.
//

#define _XOPEN_SOURCE
#define _XOPEN_SOURCE_EXTENDED 1

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <signal.h>

volatile int intr = 0;
char ch = 'A';

void catcher(int sig)
{
    if (sig != SIGUSR2) {
        intr = 1;
    } else {
        ch = 'A';
    }
}

void nextChar()
{
    if (ch < 'Z') {
        ch++;
    } else {
        ch = 'A';
    }
}

void killProcess(pid_t pid)
{
    if(kill(pid, SIGUSR1) == -1) {
        perror("Error: kill()\n");
        exit(EXIT_FAILURE);
    }
}

void childLoop(pid_t pid, sigset_t * enter, sigset_t * empty) {
    while (sigsuspend(empty) == -1 && errno == EINTR) {
        if (intr) {
            break;
        }
    }

    sigprocmask(SIG_BLOCK, enter, NULL);
    printf("Child  (%d): '%c'\n", getpid(), ch);
    nextChar();
    intr = 0;

    killProcess(getppid());

    childLoop(pid, enter, empty);
}

void childProcess(pid_t pid, sigset_t * enter, sigset_t * empty)
{
    sigemptyset(empty);

    childLoop(pid, enter, empty);
}

void parentLoop(pid_t pid, sigset_t * enter, sigset_t * empty) {
    while (sigsuspend(empty) == -1 && errno == EINTR) {
        if (intr) {
            break;
        }
    }

    sigprocmask(SIG_UNBLOCK, enter, NULL);
    printf("Press enter...");
    while (getchar() != '\n');
    sigprocmask(SIG_BLOCK, enter, NULL);

    printf("Parent (%d): '%c'\n", getpid(), ch);
    nextChar();
    intr = 0;

    killProcess(pid);

    parentLoop(pid, enter, empty);
}

void parentProcess(pid_t pid, sigset_t * enter, sigset_t * empty)
{
    printf("Parent (%d): '%c'\n", getpid(), ch);
    nextChar();
    sigemptyset(empty);

    killProcess(pid);

    parentLoop(pid, enter, empty);
}

int main(void)
{
    struct sigaction sigact;
    sigset_t enter;
    sigset_t empty;
    pid_t pid;

    sigemptyset(&enter);
    sigaddset(&enter, SIGUSR1);
    sigaddset(&enter, SIGUSR2);

    sigprocmask(SIG_BLOCK, &enter, NULL);

    sigact.sa_handler = catcher;
    sigemptyset(&sigact.sa_mask);
    sigact.sa_flags = 0;

    if (sigaction(SIGUSR1, &sigact, NULL) == -1) {
        perror("Error: sigaction() for SIGUSR1\n");
        exit(EXIT_FAILURE);
    }

    if (sigaction(SIGUSR2, &sigact, NULL) == -1) {
        perror("Error: sigaction() for SIGUSR2");
        exit(EXIT_FAILURE);
    }

    pid = fork();
    switch(pid) {
        case -1:
            perror("Error: fork()");
            exit(EXIT_FAILURE);
        case 0:
            childProcess(pid, &enter, &empty);
        default:
            parentProcess(pid, &enter, &empty);
    }
}
